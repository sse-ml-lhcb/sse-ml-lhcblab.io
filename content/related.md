---
title: "Related Projects"
date: 2017-11-06T20:33:39-05:00
---


### Other projects that might be of interest:

* [DIANA/HEP: develop state-of-the-art software tools for experiments which acquire, reduce, and analyze petabytes of data][DIANA]
* [S2I2: Conceptualization of an NSF Scientific Software Innovation Institute for High Energy Physics][S2I2]
* [RECEPT: Real-timE preCision tests of lePton universaliTy][RECEPT]


> This site was built with [Hugo] using the [Material] theme.



[DIANA]: http://diana-hep.org
[S2I2]: http://s2i2-hep.org/
[RECEPT]: http://teamlhcb-lpnhe.in2p3.fr/rubriqueRecept.html

[HUGO]: https://www.gohugo.io
[Material]: http://github.com/digitalcraftsman/hugo-material-docs"
