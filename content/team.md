---
title: "Team"
date: 2017-11-06T15:25:31-05:00
lastmod: 2018-01-04T12:16:14-05:00
---

### Team leaders:
* Mike Williams (Lead PI) -- Massachusetts Institute of Technology, Department of Physics
* Mike Sokoloff (PI) -- University of Cincinnati, Department of Physics

### Postdoctorial researchers:
* Henry Schreiner -- University of Cincinnati, Department of Physics

### Collaborators
* Mika Vesterinen -- Oxford
* Vava Gligorov -- LPNHE
* Andrey Ustyuzhanin -- Yandex
* Conor Fitzpatrick -- EPFL
* Johannes Albrecht -- TU Dortmond
* Sebastian Neubert -- Heidelberg

### Students:

* Constantin Weisser -- Massachusetts Institute of Technology, PhD student

