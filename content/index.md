---
title: "SSE: Machine Learning in the LHCb Trigger System"
date: 2017-11-06T15:13:32-05:00
type: index
---

{{< figure src="images/ssegraphic.png" alt="SSE Graphic" class="center" width="270" >}}

Flavor physics at LHCb probes the highest mass scales for beyond the Standard Model physics, and provides a unique window on dark sector physics. In the Run 3 upgrade, several major changes are being made to the rate at which the detector trigger system will need to process events. In order to handle an increase of two orders of magnitude in the events seen by the software trigger, conceptually different algorithms will need to be put in place. This project will tackle the following three
goals:

1. Use Machine learning algorithms to fully reconstruct all long tracks in HLT1
2. Use Machine learning algorithms to fully reconstruct all charged tracks in HLT2
3. Use autoencoders to reduce the number of bit stored and transferred in the system.

The algorithms and event data models we develop will be tested on a variety of emerging architectures
including "conventional" Intel Xeon processors with 512-bit vector processors, Intel's new Knight's Landing
processor, nVidia HPC GPUs, and ARM processor farms (which can incorporate GPUs along with CPUs).
In addition to developing algorithms and event data models to address the physics requirements, we
will study the (rough) life-cycle costs to deploy different hardware configurations that can support the
computing we need.

This work is funded by the NSF through the *Scientific Software Elements (SSE) grant OAC-1740102*: Collaborative Research: SI2:SSE: Extending the
284 physics reach of LHCb in Run 3 using machine learning in the real-time data ingestion and reduction
285 system.
